# !/usr/bin/env python

from distutils.core import setup

setup(
    name='FullStackTemplate',
    version='1.0.0',
    description='A Template for creating a Full Stack Web Application using Python, NPM, Webpack and React',
    author='sjk',
    author_email='sean@digitalwonderlandagency.co.uk',
    license='MIT',
    url='https://https://bitbucket.org/sjkdev/',
    keywords = [ 'fullstack', 'template', 'python', 'react','npm', 'webpack'],
    classifiers = [
    'Topic :: Software Development',
    'Natural Language :: English'
    ],
    requires=['flask']
)
